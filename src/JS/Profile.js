import React, { Component } from 'react'

import { withCookies, Cookies } from 'react-cookie'

import {Link} from 'react-router-dom'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {search} from '../Redux/Actions'
import {getProfile, setProfile} from './Requests'
const mapStateToProps = state => {return {name: state.Reducer.name}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {search: search}, dispatch)


class Profile extends Component {

  constructor(){
    super()

    this.state = {name: '',username: '' , email: '' , profile_pic: '', editing: false, newmail: 'none', newname: 'none', newprofile_pic: 'none'}
  }

  _submitProfile(){
    const {newemail: email, newname: name, newprofile_pic: profile_pic, username, editing} = this.state

    
    var newData = {}
    
    if (email != 'none')  newData = {...newData, email}
    if (name != 'none')  newData = {...newData, name}
    if (profile_pic != 'none')  newData = {...newData, profile_pic}
    
    console.log('submiting : ', newData)
    setProfile('http://communiqate.club:4005/profile',username,newData).then(res => {      
            alert('Updated profile')            
            this.setState({email, profile_pic, name})
            this.setState({editing: !editing})
    }).catch(console.log)
  }

  componentDidMount(){
    const {forDisplay = false, user} = this.props
    const username = forDisplay ? user : this.props.cookies.get('username')
    this.setState({username})
    
    this.setState({newemail: 'none'})
    this.setState({newname: 'none'})
    this.setState({newprofile_pic: 'none'})

    getProfile('http://communiqate.club:4005/profile',username).then(res => {
        // if(res.status == 200){
            const {email, profile_pic,name} = res
            this.setState({email})
            this.setState({name})
            this.setState({profile_pic})
        // }else
        //   alert('Network error')
    }).catch(console.log)
  }

  render() {
    const {name, email, profile_pic, editing} = this.state

    return (
      <div style={main}>
          <img  style={pic} src={profile_pic} />
          <div style={{display: 'flex', flexDirection:'column', alignItems: 'flex-start'}}>
            <div>Username: {!editing ? <a style={data}>{name}</a> : <input borderColor="rgba(0,0,0,0.0)" onChange={e => this.setState({newname: e.target.value})} style={inputs} placeholder={name} />} </div>
            <div>Email: {!editing ? <a style={data}>{email}</a> : <input borderColor="rgba(0,0,0,0.0)" onChange={e => this.setState({newemail: e.target.value})} style={inputs} placeholder={email} />} </div>
    {editing ? <div>Picture Url: <input borderColor="rgba(0,0,0,0.0)" onChange={e => this.setState({newprofile_pic: e.target.value})} placeholder={profile_pic} style={inputs}/> </div> : null}            
          </div>          
          <br/>
          <button name="edit" style={{...buttonStyle, visibility: !editing? "visible": "hidden"}} onClick={()=> this.setState({editing: !editing})} >Edit Profile</button>
          <button name="update" onClick={()=> this._submitProfile()} style={{...buttonStyle, visibility: editing? "visible": "hidden"}} >submit</button>
      </div>
    );
  }
}

export default connect(mapStateToProps, matchDispatchtoProps)(withCookies(Profile))


const main = {
  width: '60%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'flex-start',
  height: '110%',
  backgroundColor: 'rgba(245,245,245,0.9)'
}

const pic = {

    height: 100,
    width: 100,
    borderRadius: 100/2,
    boxShadow: '0px 0px 5px 2px pink',
    // border: 'solid 1px blue',
    margin: 50
}

const data = {
    fontSize: 25,
    fontFamilly: 'verdana',
    margin:10,
    fontWeight: 'bold'
}

const buttonStyle = {
    height: 30,
    padding: 5,
    backgroundColor: 'white',
    color: 'black',
    borderRadius: 5,
}

const inputs = {
  padding: 10,
  margin: 10,
  fontSize: 20,
  borderColor: 'rgba(0,0,0,0)'
}