import React, { Component } from 'react'
import '../App.css'

import Logo from './Logo'
import {Link} from 'react-router-dom'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {increment, search as dispatchSearch, setVisitor} from '../Redux/Actions'

const mapStateToProps = state => {return {anonymous : state.Reducer.anonymous}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {setVisitor: setVisitor}, dispatch)



class LandingPage extends Component {
  constructor(){
    super()
    this.state = {on: false, buttonOn: false}    
  }



  componentDidMount() {

    setTimeout(()=>{this.setState({on: true})} , 100)
    setTimeout(()=>{this.setState({buttonOn: true})} , 200)
    
  }


  render() {
    const {buttonOn , on} = this.state
    return (
      <div style={main(on)}>
        <Logo size= {on ? 4 : 1}/>
        <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-around', width: '50%'}}>
          <Link to ="/sign-Up"><button name="signup" style={buttonStyle(buttonOn)}>Sign-Up</button></Link>
          <Link to ="/sign-In"><button name="signin" style={buttonStyle(buttonOn)}>Sign-In</button></Link>
          <Link to ="/feed"><button name="visitor" onClick={()=>this.props.setVisitor(true)} style={buttonStyle(buttonOn)}>Visitor</button></Link>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, matchDispatchtoProps)(LandingPage)

const buttonStyle = on => {

  return {
  // width: 60,
  height: 30,
  borderRadius: 5,
  opacity: on ? 1 : 0,
  backgroundColor: 'rgba(0,0,0,0.0)',
  borderColor: 'rgba(0,0,0,0.0)',
  transition: 'all 1s ease-in-out',
  transform: on ? 'translateY(50px)' : '',
  boxShadow: '0px 1px 10px white',
  fontSize: 20,
  color: 'white'
  }
}

const main = on => {
  return {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  opacity: on ? 1 : 0,
  transition: 'all 1s ease-in-out',
  transform: on ? 'translateY(-200px)' : ''
  }
}
