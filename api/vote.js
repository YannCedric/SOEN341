const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const ObjectId = require('mongodb').ObjectId;

//using mongoose to establish a connection to the database.
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
const db = mongoose.connection;

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
/*
    To use the vote api, send an http:post request in JSON form to /vote with a body following the example below:

    {
        "questionID" : "5a87128e5557e40011a55b29",
        "answerID" : "5a87128e5557e40011a50000", // OPTIONAL: USE ONLY IF VOTING ON AN ANSWER. IF VOTING ON POST ITSELF, LEAVE BLANK
        "username" : "SuperUser",
        "voteValue" : 1
    }

    NOTE:
         1: Upvote
         0: Remove vote
        -1: Downvote
 */

app.post('/vote', bodyParser.json(), function(req, res)
{
    request = {
        username : req.body.username,
        questionID : req.body.questionID,
        answerID : req.body.answerID,
        voteValue : req.body.voteValue
    };

    // Validate request
    if (request.questionID == null)
    {
        res.json({response: 'Vote refused. Request body is missing questionID.'});
        return;
    }
    if (request.username == null)
    {
        res.json({response: 'Vote refused. Request body is missing username.'});
        return;
    }
    if (!Number.isInteger(request.voteValue) || request.voteValue < -1 || request.voteValue > 1)
    {
        res.json({response: 'Vote refused. Vote value must be -1, 0 or 1'});
        return;
    }

    db.collection('posts').findOne({_id : ObjectId(request.questionID)}, function(error, questionResult)
    {
        if (error)
        {
             res.json({response: 'An error occured'});
             return;
        }
        else if (!questionResult)
        {
            res.json({response: 'Question could not be found'});
            return;
        }
        else
        {
            // Post found
            var wVoteTarget; // Target object is either a question or an answer
            var wAnswerIndex; // Only used when voting on an answer
            if (request.answerID == null)
            {
                wVoteTarget = questionResult;
            }
            else
            {
                // Attempt to find answer
                var answerFound = false;
                for (var i = 0; i < questionResult.answers.length; i++)
                {
                    if (questionResult.answers[i]._id == request.answerID)
                    {
                        answerFound = true;
                        wAnswerIndex = i;
                        wVoteTarget = questionResult.answers[i];
                        break;
                    }
                }
                if (!answerFound)
                {
                    res.json({response: 'Answer could not be found'});
                    return;
                }
            }

            var wVotes;
            if (wVoteTarget.votes == null)
            {
                // The target object is receiving its first vote
                // Initialize "votes"
                wVotes = {tally: 0, nestedVotes: []};
            }
            else
            {
                // stringify and parse to copy
                // assignment operator doesn't copy, it assigns a reference
                wVotes = JSON.parse(JSON.stringify(wVoteTarget.votes));
            }
            var wOldTally = wVotes.tally;
            var wNestedVotes = wVotes.nestedVotes;
            var wNewVoteValue = request.voteValue;
            var wAlreadyVoted = false;
            // Iterate over nestedVotes array to see if user already voted on this post
            for (var i = 0; i < wNestedVotes.length; i++)
            {
                if (wNestedVotes[i].username == request.username)
                {
                    // Previous vote found
                    wAlreadyVoted = true;
                    var wOldVoteValue = wNestedVotes[i].voteValue;
                    if (wNewVoteValue == 0)
                    {
                        // Remove vote
                        wVotes.tally = wOldTally - wOldVoteValue;
                        wVotes.nestedVotes.splice(i, 1);
                        res.json({response : 'Vote deleted', tally: wVotes.tally});   
                    }
                    else
                    {
                        // Update vote value
                        wVotes.tally = wOldTally - wOldVoteValue + wNewVoteValue;
                        wVotes.nestedVotes[i].voteValue = wNewVoteValue;
                        res.json({response : 'Vote value updated', tally: wVotes.tally});

                    }
                    // Action already taken, stop looping through array
                    break;
                }
            }
            if (!wAlreadyVoted)
            {
                if (wNewVoteValue == 0)
                {
                    res.json({response: 'Vote could not be deleted because it was not found', tally: wVotes.tally});
                    return;
                }
                else
                {
                    // Add new vote to nestedVotes array
                    wVotes.tally = wOldTally + wNewVoteValue;
                    wVotes.nestedVotes.push({username: request.username, voteValue: wNewVoteValue});
                    res.json({response: 'Vote value set', tally: wVotes.tally});
                }
            }
            if (request.answerID == null)
            {
                // Update post votes
                db.collection('posts').update(
                    questionResult,
                    {
                        $set: {votes: wVotes}
                    }
                )
                
            }
            else
            {
                var wAnswers = JSON.parse(JSON.stringify(questionResult.answers))
                wAnswers[wAnswerIndex].votes = wVotes;
                // Update answer votes
                db.collection('posts').update(
                    questionResult,
                    {
                        $set: {answers: wAnswers}
                    }
                )
            }
            updateReputation(wVoteTarget,wVotes.tally-wOldTally);
        }
        return;
    })
    
})

//this function updates a user's reputation based on the vote provided by the voting api.
function updateReputation(post, vote)
{
    
    db.collection('users').findOne({username: post.username},function(err, postUser)
    {
        var newReputationValue=0;
       
       
        if(postUser.reputation!=null)
        var newReputationValue = postUser.reputation + vote;

       else
       {
            newReputationValue=vote;
       }

        db.collection('users').updateOne
        (
            {username: postUser.username},
            {
                $set: {reputation: newReputationValue}
            }
        )   

    });
}

//Lets the user know what port the application is starting on.
const listener = app.listen(3000, function(){ 
   console.log("Hello World! I\'m running! on port "+listener.address().port);
});