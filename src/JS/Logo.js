import React, { Component } from 'react'
import {Link} from 'react-router-dom'

export default class Logo extends Component {

    render(){
        const { size, nav=false } = this.props
        return (
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between',...main}}>
                { !nav ?
                <span style={outer(size)}>communi<span style={inner(size)}>QA</span>te</span> :
                <Link style={{textDecoration:"none"}} to="/feed" ><span style={{...inner(size),textShadow: '0px 0px 5px black', marginLeft: 15,}}>QA</span></Link> }
          </div>
        )
    }
} 

const main = {
    // backgroundColor: 'rgba(255,255,255,0.1)',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    padding: 5,
  }
const inner = (size=1) => {
    return {fontSize: 30*size,
    fontFamily: 'Impact',
    color: '#ff0066' }
  }
  const outer= (size=1) => {
    return {fontSize: 32*size,
    textShadow: '0px 0px 5px black',
    fontWeight: 'bold',
    fontFamily: 'courier',
    color: 'white'}
  }