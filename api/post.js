const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const mongodb = require('mongodb');

//using mongoose to establish a connection to the database.
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
const db = mongoose.connection;


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


const Schemas = require('./schemas');
app.post('/post', function (req, res){

    var post = new Schemas.Question();
        
    post.title = req.body.title;
    post.question = req.body.question;
    post.votes = null;
    post.answers = [];
    post.tags = req.body.tags;
    post.username = req.body.username;
    post.anonymous = req.body.anonymous;
    post.creationTime =  new Date(Date.now()).toLocaleString();

    if (isNonEmpty(post.title, "title", res)
        && isNonEmpty(post.question, "question", res)
        && isNonEmpty(post.tags, "tags", res)
        && isNonEmpty(post.username, "username", res)
        && isNonEmpty(post.anonymous, "anonymous", res))
    {
        db.collection('posts').insert(post).then(function(result){
            console.log('--> '+post.title)
            if (result.result.ok !== 1) {
                console.log(result);
                res.json({status: "error"});
                return;
            }
        });

        res.json({status: "success"});
    }
});

function isNonEmpty(value, name, res) {
    if (typeof value === 'undefined')
    {
        res.json({status: "error, " + name + " is empty or absent"})
        return false
    }
    else
    {
        return true
    }
}

/* Searching through questions, tags and usernames. */
app.post('/post/delete', function(req, res) { // Use specific path like "/search" when implementation

  if(req.body.post_id){

  db.collection('posts').deleteOne({_id : mongodb.ObjectId(req.body.post_id)}, function(err, result) {
    
    if (err) throw err;

    if(result.deletedCount == 1)
    res.json({searchStatus: 'deleted'});

    else res.json({searchStatus: 'not found'})
  });
}
  else res.json({searchStatus: 'not found'})
  
  res.end;
});

const listener = app.listen(3000, function(){ 
    console.log("Hello World! I\'m running! on port "+listener.address().port);
 });

