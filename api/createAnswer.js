const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const mongodb = require('mongodb');

//db using mongoose
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
const db = mongoose.connection;

// Init App
const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Set Port
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function(){
	console.log('Server started on port '+app.get('port'));
});

// API for creating Answer
app.post('/createAnswer', function(req, res) { 

    const Schemas = require('./schemas.js')
    const answer = new Schemas.Answer;
    
    if(req.body.username == undefined || req.body.answer == undefined || req.body.post_id == undefined) 
        res.json({updateStatus: 'Variable is Null'});   
    
    else {
    
    const post_id = mongodb.ObjectId(req.body.post_id);
    answer.answer = req.body.answer;
    answer.username = req.body.username;
    answer.anonymous = req.body.anonymous;
    answer.accepted = null;  //Always null when creating an answer, true when accepted, false if refuse. 
    answer.creationTime = new Date(Date.now()).toLocaleString();
    answer.votes = null;
   
    
        db.collection('posts').findOneAndUpdate({_id: post_id}, {$push: {answers: answer}}, function(err, result){
            if(err) throw err;
      
            if(result.value == null) 
                res.json({updateStatus: 'Not Found'});
        
            else 
                res.json({post: result, updateStatus: 'Updated'});
        });
    }
      res.end;
});