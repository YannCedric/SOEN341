const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//using mongoose to establish a connection to the database.
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
const db = mongoose.connection;


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  })
  
//For security when signing up, POST is prefered.
app.post('/signUp', function(req, res){

    //creating the user
    newUser = {
        email : req.body.email,
        userName : req.body.userName,
        password : req.body.password,
        passwordVerify : req.body.passwordVerify 
    };

    var validEmail=false;
    var validPassword=true;
    var passMatch=true;
    var validUsername= true;
    
    var emailProviders = ['hotmail','yahoo','outlook', 'gmail'];
    var emailCodes = ['.com','.ca','.net','.net','.co.uk'];

    //verifies that the email adress is valid
    for(let i=0; i<emailProviders.length;i++)
    {
        if(req.body.email.includes(emailProviders[i]))
        {
           for(let j=0; j<emailCodes.length;j++)
           {
               if(req.body.email.includes(emailCodes[j]))
               {
                   validEmail=true;
               }
           }
        }
        
    }
    if(req.body.email.indexOf('@')==0||req.body.email==''||req.body.email==' ')
    {
        validEmail=false;
    }
    if(validEmail==false)
    {
        res.json({message: 'invalid email address!', addResult: 'not added'}); 
        return;
    }

    if(req.body.userName==''||req.body.userName==' ')
    {
        res.json({message: 'invalid username!, Dont leave the field blank!', addResult: 'not added'}); 
        return;
        
    }

    if(req.body.password==''||req.body.password==' ')
    {
        res.json({message: 'invalid password!, Dont leave the field blank!', addResult: 'not added'});         
        return;
        
    }

    if(req.body.passwordVerify==''||req.body.passwordVerify==' ')
    {
        res.json({message: 'invalid password confirmation!, Dont leave the field blank!', addResult: 'not added'});
        return;                       
        
    }

    //checking if the passwords match.
    if(req.body.password!=req.body.passwordVerify)
    {
        res.json({message: 'your passwords don\'t match', addResult: 'not added'});                         
        return;
        
    }
    //doesn't create the user if any the fields contains invalid data
    if(!validEmail||!passMatch||!validPassword||!validUsername)
    {
        res.json({message: 'Invalid data', addResult: 'not added'}); 
        return;
                                        
    }
   
    //makes sure that duplicated users cannot be created.
    db.collection('users').findOne({$or: [{username: newUser.userName}, {email:newUser.email}]}, function(err, result){
      
        if (err)
        {
            res.json({addResult: 'not added', message: 'an error occured'});
            return;
        }
        if(!result)
        {
            db.collection('users').insert({username: newUser.userName, password: newUser.password, email: newUser.email});
            res.json({addResult: 'added', message:'a new user has successfully been added to the database'});
            return;
        }
        else
        {
            if(result.email==newUser.email)
            {
                res.json({addResult: 'not added', message:'This email address is already in use!'});     
                return;
                           
            }
            if(result.username==newUser.userName)
            {   
                res.json({addResult: 'not added', message:'This username is already in use! Please choose a unique username!'});            
                return;
                    
            }
        }

        
    })
  
    
});


//Lets the user know what port the application is starting on.
const listener = app.listen(3000, function(){ 
   console.log("Hello World! I\'m running! on port "+listener.address().port);
});