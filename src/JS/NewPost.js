import React, { Component } from 'react'
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import {Redirect} from 'react-router-dom'
import {post} from './Requests'
import { withCookies } from 'react-cookie'

var timestamp = require('time-stamp');

class NewPost extends Component {
    
    constructor(props) {
        super(props);
 
        this.state = {
            tags: [],
            title: '' ,
            question: ' ',
            votes: null,
            answers: null,
            done: false
            }
    }

    sendPost(){
        const { title,tags,question,votes,answers} = this.state
        const time = timestamp('YYYY:MM:DD')
        const username = this.props.cookies.get('username') 
        
        const data = {title,tags,question,votes,answers,time, username}

        post(data , 'http://communiqate.club:4003/post').then( res => {
            alert(username+' Your answer is posted'); 
            this.setState({done: true})
            this.props.reload()
        }).catch(err => alert('error , '+err))
    }

    render(){
        const {tags, done} = this.state;
        return (
        <div style={main}>
            {done ? <Redirect to="/feed" on/> : null}
            <div style={header}>
                <input placeholder={'Question Title'} style={titleInput} onChange={e => this.setState({title: e.target.value})}/>
            </div>
            
            <div style={body}>
                <textarea placeholder={'Enter your question here... '} style={messageInput} onChange={e => this.setState({question: e.target.value})}/>
            </div>
            
            <div style={tagsArea}>
                <div style={{marginRight: 10}}>Tags:</div> 
                <TagsInput value={tags} onChange={ tags => this.setState({tags})} />
            </div>

            <button style={sendButton} onClick={this.sendPost.bind(this)}>
                send
            </button>
        </div>
        )
    }
} 

export default withCookies(NewPost)

const sendButton = {
    width: 70,
    height: 20,
    boxShadow: '1px 1px 5px black',
    borderRadius: 5,
    color: 'white',
    backgroundColor: 'black',
    marginTop: 20,
}

const header = {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 30,
    padding: 10,

}
const body = {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',

}
const tagsArea = {
    ...body,
    width: '90%',
    justifyContent: 'flex-start',        
    height: 10, 
    width: '90%',   
    fontSize: 20,
    marginTop: 20,
}
const titleInput = {
    fontSize: 30,
    height: 25,
    border: '0px solid white',
    backgroundColor: 'rgba(255,255,255,0.4)',
    fontSize: 25,
    padding: 5,
    width: '70%'
}
const messageInput= {
    width: 700,
    height: 200,
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(0,0,0,0.0)',
    border: '1px solid rgba(0,0,0,0.1)',
    fontSize: 20,
    padding: 10,
    
}
const main = {
    border: '1px solid black',
    backgroundColor: 'rgba(255,255,255,0.5)',
    display: 'flex',
    flexDirection: 'column' ,
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderRadius: 5,
    fontSize: 30,
    width: 800,
    height: 360,
  }
