const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//using mongoose to establish a connection to the database.
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
const db = mongoose.connection;


app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

//For security when signing up, POST is prefered.
app.post('/signIn', function(req, res){


   

   
    db.collection('users').findOne({$and: [{username: req.body.userName}, {password:req.body.password}]}, function(err, result){
      
        if (err)
        {
            res.write("an error occured\n");
            return;
        }
        if(!result)
        {
           
            res.send('no valid user has been found');
        }
        else
        {
           res.send('Welcome,' + result.username+'\n')
        }

        
    })
  
    
});



const listener = app.listen(3000, function(){ 
   console.log("Hello World! I\'m running! on port "+listener.address().port);
});
