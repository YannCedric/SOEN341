import React, { Component } from 'react'
import { acceptRefuseAnswer } from './Requests.js'

export default class AcceptRefuseButton extends Component {
    constructor() {
        super();
        this.state = {
            Accept: false,
            Neutral: true,
            Refuse: false,
            answerId
        };

        this.handleAccept = this.handleAccept.bind(this);
        this.handleNeutralNotAccept = this.handleNeutralNotAccept.bind(this);
        this.handleNeutralNotRefuse = this.handleNeutralNotRefuse.bind(this);
        this.handleRefuse = this.handleRefuse.bind(this);
    } 

    componentDidMount(){
        this.setState(answerId = this.props._id).bind(this);
    }

    handleAccept(){
        this.setState({
            Accept: true,
            Neutral: false,
            Refuse: false
        });
        var status = 1;
        acceptRefuseAnswer({answerId, status}, 'http://communiqate.club:4004/acceptRefuse');
    }

    handleNeutralNotAccept(){
        this.setState({
            Accept: false,
            Neutral: true,
            Refuse: false
        });
        var status = 0;
        status = NULL;
        acceptRefuseAnswer({answerId, status}, 'http://communiqate.club:4004/acceptRefuse');
    }

    handleNeutralNotRefuse(){
        this.setState({
            Accept: false,
            Neutral: true,
            Refuse: false
        });
        var status = 0;
        status = NULL;
        acceptRefuseAnswer({answerId, status}, 'http://communiqate.club:4004/acceptRefuse');
    }

    handleRefuse(){
        this.setState({
            Accept: false,
            Neutral: false,
            Refuse: true
        });
        var status = 0;
        acceptRefuseAnswer({answerId, status}, 'http://communiqate.club:4004/acceptRefuse');
    }

    render() {
        if (this.state.Neutral) {
            return (
            <div>
                <div>
                    <button title="Accept an answer to close the thread" style={accept} onClick={this.handleAccept}>
                        Accept
                    </button>
                </div>
                <div>
                    <button title="Refuse an answer if misleading or clearly wrong" style={refuse} onClick={this.handleRefuse}>
                        Refuse
                    </button>
                </div>
            </div>
            );
        }
        else if (this.state.Accept) {
            return (
            <div>
                <button title="Click here to retract the acceptance of this answers" style={cancelAccept} onClick={this.handleNeutralNotAccept}>
                    Accepted
                </button>
            </div>
            );
        }
        else if (this.state.Refuse) {
            return (
            <div>
                <button title="Click here to retract the refusal of this answer" style={cancelRefuse} onClick={this.handleNeutralNotRefuse}>
                    Refused
                </button>
            </div>
            );
        }
    }
}

const cancelAccept = {
    fontSize: 18,
    backgroundColor: '#3ed160',
    color: 'white',
    border: 1,
    flex: 1,
    borderRadius: 30
}

const cancelRefuse = {
    fontSize: 18,
    backgroundColor: '#ce433b',
    color: 'white',
    border: 1,
    flex: 1,
    borderRadius: 30
}

const accept = {
    fontSize: 14,
    backgroundColor: '#728c69',
    color: 'white',
    border: 1,
    flex: 1,
    borderRadius: 30
}

const refuse = {
    fontSize: 14,
    backgroundColor: '#916d6d',
    color: 'white',
    border: 1,
    flex: 1,
    borderRadius: 30
}