//import { ObjectID } from '../.cache/typescript/2.6/node_modules/@types/bson';

var mongoose = require('mongoose')
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var ObjectID = require('mongodb').ObjectID; 
var mongodb = require('mongodb');

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
      
//connect to the database
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
var db = mongoose.connection;

// Init App
const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/acceptRefuse', function(req, res){


var answerId = mongodb.ObjectId(req.body.answerId);

var status = req.body.status;
var statusBoolean =null;

if(status == 1)
statusBoolean =true;
else if(status == 0)
statusBoolean =false;

db.collection('posts').update( { "answers._id"  : answerId  
},
{
  $set: { "answers.$.accepted": statusBoolean}

});
res.end();

   
});

app.listen(3007);
console.log('Listening on port 3007...');







