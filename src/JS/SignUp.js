import React, { Component } from 'react'
import '../App.css'

import Logo from './Logo'
import {Link} from 'react-router-dom'
import { signUp as signUpMethod } from './Requests'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {setVisitor} from '../Redux/Actions'

const mapStateToProps = state => {return {visitor: state.Reducer.visitor}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {setVisitor: setVisitor}, dispatch)

const Recaptcha = require('react-recaptcha');


class SignUp extends Component {
    constructor(){
        super()
        this.state = { mounted: false , 
            email : ' ',
            userName : ' ',
            password : ' ',
            passwordVerify : ' ',
            isHuman: false
        }
        
    }
    componentDidMount(){
        setInterval(()=> this.setState({mounted: true}), 100)
    }

    processSignup(){
        const {email, userName , password, passwordVerify, isHuman} = this.state
        const { reviewedTerms } = this.refs

        if(isHuman, reviewedTerms.checked)
            signUpMethod({email, userName, password, passwordVerify}, 'http://communiqate.club:4001/signup')
            .then(res => {
                if(res.addResult == 'added'){
                    alert('Welcome, ' + userName)
                    this.props.setVisitor(false)
                    this.props.onSignUp(userName)
                }
                else if(res.addResult == 'not added'){
                    alert(res.message)
                }
            }).catch(alert)
        else if(!isHuman) alert('Please make sure you are human')
        else if(!reviewedTerms.checked) alert('Please review our terms and conditions')
    }

  render() {
      const { mounted } = this.state
    return (
      <div style={ main(mounted) }>
        <Logo/>
        <div style={box}>
            <a style={labels}>Email</a>
            <input placeholder="example@test.lol" style={input} onChange={e => this.setState({email: e.target.value})}/>  

            <a style={labels}>Username</a>
            <input placeholder="superman" style={input} onChange={e => this.setState({userName: e.target.value})} />

            <a style={labels}>Password</a>
            <input placeholder="try 'password'" type="password" style={input} onChange={e => this.setState({password: e.target.value})}/>

            <a style={labels}>Password (again)</a>
            <input placeholder="just kidding.. don't" type="password" style={input} onChange={e => this.setState({passwordVerify: e.target.value})}/>

            <button style={button} onClick={() => this.processSignup() }>
                sign-up
            </button>

            <Link to="/sign-in" style={{fontSize: 15,}}>I have an account</Link>

            <div style={separator}/>
            <div style={terms} flexdirection="row">
                <input type="checkbox" ref="reviewedTerms"/> I definately reviewed the <a href="" style={{color: 'blue'}}>Terms and Conditions</a>
            </div>
            
            <Recaptcha
                sitekey={sitekey}
                render="explicit"
                verifyCallback={()=> this.setState({isHuman: true})}
            />
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, matchDispatchtoProps)(SignUp)

const sitekey = "6LcuMkMUAAAAAEhOnaMbF_y7CCHPFG501Y2z7KrM"
// const privateKey = "6LcuMkMUAAAAANwEqHh1FGwKP4Qm2ZPnMwHGqv2Z"

const separator = {
    height: 0.5,
    width: '60%',
    backgroundColor: 'black',
    opacity: 0.2,
    // boxShadow: '0px 0px 3px black',   
}

const labels={
    fontSize: 14,
}

const terms = {
    fontSize: 10,
}

const main = (on) => {
    return { display: 'flex',
    flexDirection: 'column',
    justiFyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    opacity: on ? 1 : 0,
    transition: 'all 0.5s ease-in-out',
    transform: on ? 'translateY(10px)' : ''
    }
}


const box = {
    marginTop: 10,
    width: 300,
    height: 510,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    border: '1px solid rgba(0,0,0,0)',
    boxShadow: '0px 0px 1px black',
    padding: 5,
    borderRadius: 2,
    fontFamily: 'Helvetica',
    backgroundColor: 'rgba(255,255,255,0.8)'
}

const input = {
    width: '80%',
    borderRadius: 3,
    height: 20,
    fontSize: 15,
    padding: 5,
}
 
const button = {
    height: 30,
    width: '80%',
    borderRadius: 5,
    fontSize: 15,
    // boxShadow: '0px 0px 10px black',
    

}