import React, { Component } from 'react';
import  {searchPosts} from './Requests'
import Answer from './Answer'
import {Link} from 'react-router-dom'
import ReadAnswer from './ReadAnswer'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {setVisitor} from '../Redux/Actions'

const mapStateToProps = state => {return {name: state.Reducer.name,visitor: state.Reducer.visitor}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {setVisitor: setVisitor}, dispatch)

class Post extends Component {
    constructor(){
        super();
        this.state = { post: {tags: []}, answers: [], commentsloading: false
        }
    
    }

    

    componentWillMount(){
        this._fetchPost()
    }

    _fetchPost(){ 
        console.log('Loading...')
        const {matching = true} = this.props
        const { _id } = matching ? this.props.match.params : this.props

        this.setState({loading: true})
        searchPosts( '_id' , 'http://communiqate.club:4000/search',_id).then(res => {
            console.log(' this is one post ', res.post[0])
            this.setState({loading: false})            
            this.setState({post: res.post[0]})
        }).catch( err => {alert('problem fetching')} )
    }

    buildTags(bunchatags) {
        var tmpTags = [""];
        for (var i = 0; i < bunchatags.length; i++){
            tmpTags.push(<a style={tagStyle}> {bunchatags[i]} </a>);
        }
        this.setState({tags: tmpTags}); 
    }

    render() {
        const {withAnswers = true, visitor = false} = this.props
        const { _id ,title , question : question, tags, username = '', answers} = this.state.post
        const {loading} = this.state
        return (
            <div style={box}>
                <div style={postSection}>
                    {loading ? 'Loading' : null}
                    <div style={{display: 'flex', justifyContent: 'space-between'}}><Link to={`/feed/post/${_id}`} style={{textDecoration:"none"}} ><h1 style={{color: 'black'}}>{title}</h1></Link> <div style={{color:"black"}}>{username}</div></div>
                    <p>{question}</p>
                    <hr width="100%" />
                    <div style={tagSection}>
                        <div style={{marginRight: 10, alignSelf: 'center'}} >Tags: </div> { tags ? tags.map(item => (<div style={tag}>{item}</div>)) : null}
                    </div>
                </div>
                { withAnswers && answers ? 
                    (<div style={answersSection}>
                        {answers.map(ans => <Answer author={this.props.name} tally={ans.votes ? ans.votes.tally : 0} post_id={_id} ans_id={ans._id} username={ans.username} visitor={visitor} reload={ () => { this._fetchPost() } } username={ans.username} answer={ans.answer}/> )}
                    </div>)
                    : null }
                { withAnswers && !visitor ? <ReadAnswer reload={()=>{ this._fetchPost()}} post_id={_id}/> : null}
                

            </div>
        );
    }
}

export default connect(mapStateToProps, matchDispatchtoProps)(Post)

const postSection = {
    padding: 10,
    borderRadius: 4,
    boxShadow: '1px 6px 1px lightgrey',
    marginBottom: 20,
    backgroundColor: 'white',
    // border: 'solid rgba(0,0,0,0.2)',
    // borderWidth: 1
} 
 
const tag= {
    backgroundColor: 'black',
    borderRadius: 8,
    padding: 5,
    color: 'white',
    marginRight: 5,
}
const answersSection = {
    width: '100%',
    display:'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 5,
    borderLeft: '1px solid lightgrey',
    borderRight: '1px solid lightgrey',
    marginBottom: 400,
}

const box = {
    flex: 1,
    marginTop: 10,
    borderRadius: 2,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 5,
    fontFamily: 'Arial',
    width: '80%'
}

const tagSection = {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
}

const tagStyle = {
    fontSize: 14,
    backgroundColor: '#dad5f2'
}
