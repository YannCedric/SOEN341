
var express  = require('express');
var app      = express();
var port     = process.env.PORT || 3000;
var mongoose = require('mongoose');
var passport = require('passport');
var localStrategy = require('passport-local');

var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session = require('express-session');

// var dbConfig = require('./config/dbConfig');
// mongoose.connect(dbConfig.url);
// var db = mongoose.connection;

//Sessions setup
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

//BodyParser setup
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

//Passport init
app.use(passport.initialize());
app.use(passport.session());

//Database setup
module.exports = mongoose.model('User',{
    id: Number,
    username: String,
    password: String,
    email: String,
    reputation: Number
});

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

//Frontend serving
app.use(express.static('build'));
app.get('/', express.static('build'));

//Routes
// require('./api/routes')(app, passport, db, bodyParser);

//Server
app.listen(port, function(){ console.log('Server started on port ' + port);});
