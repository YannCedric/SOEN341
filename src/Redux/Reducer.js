export default function Reducer(state={value: 0, searching: false, logged: false, userName: '', visitor: false},action) {
    switch (action.type) {
        case 'INCREMENT':
            return {...state, value: state.value + action.payload};
        case 'SEARCH':
            console.log('searchingg...')
            return {...state, searching: action.payload};
        case 'LOGIN':
            console.log('loggin')
            return {...state, logged: true, userName: action.payload};
        case 'LOGOUT':
            return {...state, logged: false, userName: ''};
        case 'VISITOR':
            return {...state, visitor: action.payload};
        default :
            return state;
    }
  }