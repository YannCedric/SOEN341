var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

//db using mongoose
var mongoose = require('mongoose');
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
var db = mongoose.connection;

// Init App
var app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Set Port
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function(){
	console.log('Server started on port '+app.get('port'));
});

/* Searching through questions, tags and usernames. */
app.post('/search', function(req, res) { // Use specific path like "/search" when implementation
  const search = req.body.search;
  const _id = req.body._id;
 
  // if(search){
  console.log("Searching for: %s", search);
  
  const regex = search == '_id' ? _id : new RegExp(search, 'i'); //Case insensitive
 
  //Search through questions
  //creator: user of the post created
  db.collection('posts').find({ $or: [ { question: regex }, { tags: regex }, {username: regex}, {_id: mongoose.Types.ObjectId(search == '_id' ? regex : null)}, {creator: regex}] }).toArray(function(err, result) {
    //Check for error
    if (err) throw err;
 
    if(!result.length ==0)
    res.json({post: result, searchStatus: 'found'});
    
    else res.json({searchStatus: 'not found'})
  });
// }
//   else res.json({searchStatus: 'not found'})
  
  res.end;
});
