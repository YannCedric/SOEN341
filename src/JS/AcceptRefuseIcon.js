import React, { Component } from 'react'

export default class AcceptRefuseIcon extends Component {
    render(){
        if (this.props.accepted === "true"){
            return (
                <h6 style={Accepted}>Accepted</h6>
            )
        }
        else if (this.props.accepted === "false"){
            return (
                <h6 style={Refused}>Refused</h6>
            )
        }
        else {
            return (
                <h6 style={Neutral}>Not Accepted</h6>
            )
        }
    }
}

const Accepted = {
    color: 'white',
    backgroundColor: '#3ed160',
    borderRadius: 8,
    padding: 5
}

const Refused = {
    color: 'white',
    backgroundColor: '#d13e3e',
    borderRadius: 8,
    padding: 5
}

const Neutral = {
    color: 'grey',
    borderRadius: 8,
    padding: 5
}