import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Navbar from './Navbar'
import axios from 'axios'
import NewPost from './NewPost'
import SearchResult from './SearchResult'
import Profile from './Profile'
import  {searchPosts} from './Requests'
import Post from './Post'
import Carousel from 'nuka-carousel'

import {HashRouter as Router, Route} from 'react-router-dom'

const images = [
    "https://coda.newjobs.com/api/imagesproxy/ms/cms/content30/images/question-marks-color.jpg",
    "https://www.elegantthemes.com/blog/wp-content/uploads/2015/01/Feature-Image-Most-Popular-Posts-2015.gif",
    "http://www.tradeready.ca/wp-content/uploads/2017/12/most-popular-global-trade-articles-2017.jpg",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiKGkgP3yoBrbpfwISEZWv7uSMbmNkSXyE-x5uC3P3NCM5A8ICTA",
    "https://ponderingprinciples.com/wp-content/uploads/2017/12/Political-Speech.jpg",
    "https://cdn-images-1.medium.com/max/825/1*CDlclChuNeeM5Shfev2RTg.jpeg"
  ];


export default class Feed extends Component {

    constructor(){
        super()

        this.state = { loading: false, posts: [] }
    }

    _fetchPosts(){ 
        const {posts} = this.state

        searchPosts('', 'http://communiqate.club:4000/search').then(res => {
            console.log(res)            
            this.setState({posts: [...posts,...res.post].reverse()})
        }).catch( err => {alert('problem fetching')} )
    }

    componentDidMount(){
        this._fetchPosts()
    }

    render(){
        const { posts  } = this.state
        const { username  } = this.props
        return (
            <div id="feedsection" style={main}>
                <Navbar/>
                <div style={{height:80}}/>
 
                <Router>
                <div style={feed}>
                    <Route exact path="/feed" component={ ()=> { 
                        return (
                            <div id="posts"  style={{height: '50%'}}>
                                <h1 style={header} >Checkout our sections</h1>                                
                                    <Carousel easing="easeInQuad" autoplay={true} wrapAround={true} autoplayInterval={2000} slidesToShow={2} framePadding='20px' cellSpacing={10} style={{height: '100%' ,width: 870, textAlign: 'center', fontFamily: 'verdana'}} >
                                        {/* {images.map(item => <img style={{alignSelf: 'center' ,height: 200, width: 400, margin: 5}} src={item}/> )} */}
                                        <div>
                                            Discovery Mode
                                            <img style={{alignSelf: 'center' ,height: 200, width: 400, margin: 5}} src={images[0]}/>
                                        </div>
                                        <div>
                                            Most Aproved Questions
                                            <img style={{alignSelf: 'center' ,height: 200, width: 400, margin: 5}} src={images[1]}/>
                                        </div>
                                        <div>
                                            Tech Related
                                            <img style={{alignSelf: 'center' ,height: 200, width: 400, margin: 5}} src={images[5]}/>
                                        </div>
                                        <div>
                                            Digital School
                                            <img style={{alignSelf: 'center' ,height: 200, width: 400, margin: 5}} src={images[2]}/>
                                        </div>
                                        <div>
                                            Controversial
                                            <img style={{alignSelf: 'center' ,height: 200, width: 400, margin: 5}} src={images[3]}/>
                                        </div>
                                        <div>
                                            Politics
                                            <img style={{alignSelf: 'center' ,height: 200, width: 400, margin: 5}} src={images[4]}/>
                                        </div>
                                        
                                    </Carousel>
                                <h1 style={header} >Recently Posted Questions</h1>
                                <div style={{flexDirection: 'column', display:'flex', justifyContent: 'space-around', alignItems: 'center',width:'100%'}}> 
                                {posts.filter(x => x.username != 'TestUser').filter(x => !x.title.toLowerCase().includes('test')).map( item => <Link style={{textDecoration: 'none', color: 'black' ,width:'100%',flexDirection: 'column', display:'flex', alignItems: 'center'}} to={`/feed/post/${item._id}`}> 
                                                        <div style={display}>Title: <a style={{fontSize: 25, fontFamily: 'verdana', fontWeight: 'normal', fontStyle: 'normal'}}>{item.title}</a> <br/> from <a style={{fontSize: 20, fontFamily: 'arial', fontWeight: 'normal',fontStyle: 'normal', textAlign: 'right'}}>{item.username}</a></div> 
                                                    </Link>)}
                                </div>
                            </div>)
                    }}/>
                    <Route exact path="/feed/post/:_id" component={Post}/>
                    <Route exact path="/feed/newpost" component={ () => <NewPost reload={ ()=> {this._fetchPosts()} } username={username}/>}/>
                    <Route exact path="/feed/search/:key" component={SearchResult}/>
                    <Route exact path="/feed/profile" component={Profile}/>
                </div>
                </Router>                
            </div>
        )
    }
} 

const main = {
    backgroundColor: 'rgba(255,255,255,0.95)',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',   
    padding: 10,
    width: '90%',
    height: '100%',
    overflow: 'auto'
  }
const display = {
    padding: 5,
    minHeight: 20,
    fontWeight: 'bold',
    margin: 5,
    fontSize: 15,
    width: '70%',
    // boxShadow: '0px 0px 1px lightgrey', 
    borderRadius: 5,
    borderLeft: '1px solid black',
    borderBottom: '1px solid black',
    // backgroundColor:'white',
    // textAlign: 'center',
    fontStyle: 'italic',
}

  const header = {
      fontSize: 40,
      padding: 20,
      fontFamily: 'palatino',
      textAlign: 'center'
  }

  const feed = {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }
 