const chaiAsPromised = require('chai-as-promised')
const chai = require('chai')
const {searchPosts : search , signUp, signIn, post, sendAnswer, getProfile} = require('../src/JS/Requests')


// chai.use(chaiAsPromised)
// const assert = require("chai").assert;

const expect = require("chai").expect;
const should = require("chai").should();



const prodFlag = process.argv[3]
const search_url = prodFlag === "--prod" ? 'http://communiqate.club:4000/search' : 'http://search:3000/search'
const signUp_url = prodFlag === "--prod" ? 'http://communiqate.club:4001/signUp' : 'http://signup:3000/signUp'
const signIn_url = prodFlag === "--prod" ? 'http://communiqate.club:4002/signIn' : 'http://signin:3000/signin'
const post_Url = prodFlag === "--prod" ? 'http://communiqate.club:4003/post' : 'http://post:3000/post'
const newAnswer_url = prodFlag === "--prod" ? 'http://communiqate.club:4004/createAnswer' : 'http://answers:3000/createAnswer'
const profile_url = prodFlag === "--prod" ? 'http://communiqate.club:4005/profile' : 'http://profile:3000/profile'

console.log('Flag: ',prodFlag || 'none')

describe('Search API', function() {
       
        it('should resolve to an object', async () => {
            await search('java', search_url).then(  () => 
                data => expect(data).to.be.a('object') ).catch( pleaseNoError )
        })

        it('should contain the property searchStatus', async () => {
            await search('java', search_url).then(
                data => expect(data).to.have.property('searchStatus') ).catch( pleaseNoError )
        })

        it('should return an array of results', async () => {
           await search(' ', search_url).then(
               data => expect(data.post).to.be.an('array') ).catch( pleaseNoError )
        })
        
        //Search for word that isn't in database
        it('should not find the word \'ktm\'', async () => {
            await search('ktm', search_url).then(
                data => expect(data.searchStatus).to.be.equal('not found')).catch( pleaseNoError )
        })

        //Search for empty space
        it('empty case should not be \'not found \'', async () => {
            await search('', search_url).then(
                data => expect(data.searchStatus).to.not.be.equal('not found')).catch( pleaseNoError )
        })
});


const userMock = {
        email : 'jesus@gmail.com',
        userName : 'i_said_jesus',
        password : 'secret',
        passwordVerify : 'secret'            
} 


describe('Sign-up API', function() {

    it('should resolve to an object', async ()=> {
        await signUp(userMock, signUp_url).then( 
            data => expect(data).to.be.a('object') ).catch( pleaseNoError )
    })

    it('should contain the property addResult', async ()=> {
        await signUp(userMock, signUp_url).then(
            data => expect(data).to.have.property('addResult') ).catch( pleaseNoError )
    })

    it('should contain the property message', async ()=> {
        await signUp(userMock, signUp_url).then(
            data => expect(data).to.have.property('message') ).catch( pleaseNoError )
    })

    it('should verifies exising username', async ()=> {
        await signUp(userMock, signUp_url).then(
          data => expect(data.addResult).to.be.equal('not added') ).catch( pleaseNoError )
    })

    it('should verifies non-matching passwords', async ()=> {
        await signUp({...userMock, password: 'doesthiswork?'}, signUp_url).then(
          data => {
              expect(data.addResult).to.be.equal('not added') 
              expect(data.message).to.be.equal('your passwords don\'t match')
        }).catch(pleaseNoError )
    })

    it('makes sure a user isn\'t added if the username is blank', async()=>{
        await signUp({...userMock,  userName:' ' }, signUp_url).then(
            data=>{
                expect(data.addResult).to.be.equal('not added')
                expect(data.message).to.be.equal('invalid username!, Dont leave the field blank!')
            }).catch(pleaseNoError)
    })
    it('Data entered to signup should resolve to strings', async ()=> {
        await signUp(userMock, signUp_url).then( 
            data => {
                expect(userMock.userName).to.be.a('string') ,
                expect(userMock.email).to.be.a('string') ,
                expect(userMock.password).to.be.a('string') ,
                expect(userMock.passwordVerify).to.be.a('string')
             }).catch(pleaseNoError)
    })



    it('makes sure a user isn\'t added if the password is blank', async()=>{
        await signUp({...userMock, password:' ' }, signUp_url).then(
            data=>{
                expect(data.addResult).to.be.equal('not added')
                expect(data.message).to.be.equal('invalid password!, Dont leave the field blank!')
            }
        ).catch(pleaseNoError)
    })

    it('makes sure a user isn\'t added if the email is blank', async()=>{
        await signUp({...userMock, email:' ' }, signUp_url).then(
            data=>{
                expect(data.addResult).to.be.equal('not added')
                expect(data.message).to.be.equal('invalid email address!')
            }
        ).catch(pleaseNoError)
    })

    it('makes sure a user isn\'t added if the email is invalid', async()=>{
        await signUp({...userMock, email: 'a@.com'}, signUp_url).then(
            data=>{
                expect(data.addResult).to.be.equal('not added')
                expect(data.message).to.be.equal('invalid email address!')  
            }
        ).catch(pleaseNoError)
    })

    it('makes sure the password confirmation field is not blank', async()=>{
        await signUp({...userMock, passwordVerify: ' '}, signUp_url).then(
            data=>{
                expect(data.addResult).to.be.equal('not added')
                expect(data.message).to.be.equal('invalid password confirmation!, Dont leave the field blank!')  
            }
        ).catch(pleaseNoError)
    })
   
});


const loginMock = {
    userName : 'bbc',
    password: 'pass'
} 

describe('Sign-in API', function() {

    it('should resolve to a string', async ()=> {
        await signIn(loginMock, signIn_url).then( 
            res => expect(res).to.be.a('string') ).catch( pleaseNoError )
    })

    it('should say welcome to existing user', async ()=> {
        await signIn(loginMock, signIn_url).then(
            data => expect(data).to.contain('Welcome') ).catch( pleaseNoError )
    })

    it('should say user is invalid for non existing credentials', async ()=> {
        await signIn({...loginMock, password: 'wrong password' }, signIn_url).then(
          data => expect(data).to.contain('no valid user') ).catch( pleaseNoError )
    })
});

const postMock = {
    title : 'Testing Title',
    question: 'Im a testing title',
    tags: ["tag1", "tag2", "tag3"],
    username: 'TestUser',
    anonymous: false
}

const postMockFailure = {
    title : 'Testing Title Fail',
    question: 'Im a testing title',
    tags: ["tag1", "tag2", "tag3"],
    anonymous: false
}

describe('Post API', function() {

    it('should resolve to an object', async ()=> {
        await post(postMock, post_Url).then(
            res => expect(res).to.be.a('object') ).catch( pleaseNoError )
    })

    it('should have property status', async ()=> {
        await post(postMock, post_Url).then(
            data => expect(data).to.have.property('status') ).catch( pleaseNoError )
    })

    it('should return \'success\' for valid question', async ()=> {
        await post(postMock, post_Url).then(
            data => expect(data.status).to.equal('success') ).catch( pleaseNoError )
    })

    it('should return an error for missing fields', async ()=> {
        await post(postMockFailure, post_Url).then(
            data => expect(data.status).to.contain('error')).catch(pleaseNoError)
    })
});


const newAnswerMock = {
    username : 'test',
    answer: 'Im a testing new answer',
    post_id: '5a996bbd40bb890011577399' //Should not be found.
} 

const newAnswerMockNull = {
    username : 'Jesus',
    answer: 'Im a testing new answer',
    post_id: '5a996bbd40bb890011577348'
} 

describe('New Answer API', function() {

    it('should resolve to an object', async ()=> {
        await sendAnswer(newAnswerMock, newAnswer_url).then( 
            res => expect(res).to.be.a('object') ).catch( pleaseNoError )
    })

    it('should have property updateStatus', async ()=> {
        await sendAnswer(newAnswerMock, newAnswer_url).then(
            data => expect(data).to.have.property('updateStatus') ).catch( pleaseNoError )
    })

    it('should return \'Not Found\' for non existing post', async ()=> {
        await sendAnswer(newAnswerMock, newAnswer_url).then(
            data => expect(data.updateStatus).to.contain('Not Found') ).catch( pleaseNoError )
    })

    it('should return \'Variable is Null\' for non existing answer', async ()=> {
        newAnswerMockNull.answer = undefined;
        await sendAnswer(newAnswerMockNull, newAnswer_url).then(
            data => expect(data.updateStatus).to.contain('Variable is Null') ).catch( pleaseNoError )
            //Set variable back to original
            newAnswerMockNull.answer = 'Im a testing new answer';
    })

    it('should return \'Variable is Null\' for non existing username', async ()=> {
        newAnswerMockNull.username = undefined;
        await sendAnswer(newAnswerMockNull, newAnswer_url).then(
            data => expect(data.updateStatus).to.contain('Variable is Null') ).catch( pleaseNoError )
            //Set variable back to original
            newAnswerMockNull.username = 'Jesus';
    })

    it('should return \'Variable is Null\' for non existing post_id', async ()=> {
        newAnswerMockNull.post_id = undefined;
        await sendAnswer(newAnswerMockNull, newAnswer_url).then(
            data => expect(data.updateStatus).to.contain('Variable is Null') ).catch( pleaseNoError )
        //Set variable back to original
        newAnswerMockNull.post_id = '5a996bbd40bb890011577348';    
    })
});

const profileMock = {
    email : 'yanncedrik@gmail.com',
    name: 'bbc',
    profile_pic: ''
} 
describe('Profile API', function() {

    it('should resolve to an object', async ()=> {
        await getProfile(profile_url,profileMock.name).then( 
            res => expect(res).to.be.a('object') ).catch( pleaseNoError )
    })
    it('should resolve to an object', async ()=> {
        await getProfile(profile_url,profileMock.name).then( 
            res => expect(res.email).to.be.equal(profileMock.email) ).catch( pleaseNoError )
    })
});




function pleaseNoError( err ) {  console.log('-----------Error Start----------- \n' + err +'\n -----------Error End-----------'); should.equal(err, undefined)  }