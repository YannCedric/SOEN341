import React, { Component } from 'react'
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css'
import {Redirect} from 'react-router-dom'
import {post} from './Requests'
import { withCookies } from 'react-cookie'
import {sendAnswer} from './Requests'
var timestamp = require('time-stamp');

class NewPost extends Component {
    
    constructor(props) {
        super(props);
 
        this.state = {
            answer: '',
            }
    }
    sendPost(){
        const {answer} = this.state
        const username = this.props.cookies.get('username') 
        const {post_id} = this.props

        const data = {post_id, answer,username}

        post(data , 'http://communiqate.club:4004/createanswer').then( res => {
            alert(username+' Your answer is posted');
            setTimeout(()=> {
                this.props.reload()
            }, 300) 
        }).catch(err => alert('error , '+err))
    }

    render(){
        const {tags, done} = this.state;
        return (
        <div style={main}>
            <div style={header}>
                <div>{'Answer'}</div>
            </div>
            <div style={body}>
                <textarea placeholder={'Enter your answer here... '} style={messageInput} onChange={e => this.setState({answer: e.target.value})}/>
            </div>
            <button style={sendButton} onClick={this.sendPost.bind(this)}>
                answer
            </button>
        </div>
        )
    }
} 

export default withCookies(NewPost)

const sendButton = {
    height: 30,
    borderRadius: 5,
    color: 'white',
    backgroundColor: '#ff0066',
    fontSize: 20,
    alignSelt: 'flex-end',
}

const header = {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    alignItems: 'flex-start',
    paddingLeft: 20,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 15,

}
const body = {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
}

const messageInput= {
    width: '90%',
    height: '90%',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: 'rgba(0,0,0,0.0)',
    border: '1px solid rgba(0,0,0,0.1)',
    fontSize: 20,
    padding: 10,
}
const main = {
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'column' ,
    alignItems: 'center',
    justifyContent: 'flex-around',
    borderRadius: 3,
    fontSize: 30,
    width: '73%',
    height: 170,
    position: 'absolute',
    bottom: 10,
    boxShadow: '1px 2px 1px lightgrey',    
    
  }
