import React, { Component } from 'react'
//import { getProfile, sendProfile } from './Requests.js'
import { instanceOf } from 'prop-types'
import { withCookies, Cookies } from 'react-cookie'

class EditProfile extends Component {
    constructor(){
        super()
        this.state = { profile: { username: '', password:  '' , email: '', avatar: '' },
                         new_username: '', new_password: '', new_avatar: '', new_email: '' }
        this.handleSubmitUser = this.handleSubmitUser.bind(this)
        this.handleSubmitPassword = this.handleSubmitPassword.bind(this)
        this.handleSubmitEmail = this.handleSubmitEmail.bind(this)
        this.handleSubmitAvatar = this.handleSubmitAvatar.bind(this)
        this.handleEditUser = this.handleEditUser.bind(this)
        this.handleEditPassword = this.handleEditPassword.bind(this) 
        this.handleEditEmail = this.handleEditEmail.bind(this) 
        this.handleEditAvatar = this.handleEditAvatar.bind(this) 
    }
    
    static proTypes = {
        cookies: instanceOf(Cookies).isRequired
    };

    componentDidMount(){
        const { cookies } = this.props
        const username = cookies.get('username')
        console.log("uname: " + username);
        //this._fetchProfile(username)
        this.setState({ profile: { username: username, password: "dangerous", email: "something@club.com", avatar: "http://static.pexels.com/photos/14621/Warsaw-at-night-free-license-CC0.jpg" }}) //for testing
    }

    _fetchProfile(username){//send username, get all
        /*getProfile({username}, "http:").then(res => {
            console.log(res)            
                this.setState(profile: res.profile) //expecting the full profile
            }).catch( err => {alert('problem fetching')} )*/
    }

    handleEditUser(e) {
        this.setState({ new_username: e.target.value })
    }

    handleSubmitUser() {
        /*let profile = Object.assign({}, this.state.profile)
        profile.username = this.state.new_username
        sendProfile({ profile }, "http")*/
        //for testing
        let profile = Object.assign({}, this.state.profile)
        profile.username = this.state.new_username
        this.setState({ profile })
    }

    handleEditPassword(e) {
        this.setState({ new_password: e.target.value })
    }

    handleSubmitPassword() {
        /*let profile = Object.assign({}, this.state.profile)
        profile.password = this.state.password
        sendProfile({ profile }, "http")*/
    }

    handleEditEmail(e) {
        this.setState({ new_email: e.target.value })
    }

    handleSubmitEmail() {
        /*let profile = Object.assign({}, this.state.profile)
        profile.email = this.state.new_email
        sendProfile({ profile }, "http")*/
    }

    handleEditAvatar(e) {
        this.setState({ new_avatar: e.target.value })
    }

    handleSubmitAvatar() {
        /*let profile = Object.assign({}, this.state.profile)
        profile.avatar = this.state.new_avatar
        sendProfile({ profile }, "http")*/
    }

    render(){
        const { username, password, email, avatar } = this.state.profile
        return (
            <div style={section}>
                <div style={view}>
                <h4>Current Profile</h4>
                    <div style={entry}>
                        <div> Avatar:</div> <img src={avatar} alt="Failed to load image!!" height='84' width='84' />
                    </div><br/>
                    <div style={entry}>
                        <label> Username:</label> {username}
                    </div>
                    <div style={entry}>
                        <label> Password:</label> {password} {/* showing password like this may be unwise*/}
                    </div>
                    <div style={entry}>
                        <label> Email:</label> {email}
                    </div>
                </div>
                <div style={form}>
                <h4>Enter new profile here</h4>
                    <div style={entry}>
                        <div> New Username: </div> <input type="text" onChange={ this.handleEditUser } />
                        <input style={{ borderRadius: 30, color: 'white', backgroundColor: 'black' }} type="button" value="submit" title="Click here to update your username." onClick={ this.handleSubmitUser } />
                    </div>
                    <div style={entry}> 
                        <div> New Password: </div> <input type="text" onChange={ this.handleEditPassword } />
                        <input style={{ borderRadius: 30, color: 'white', backgroundColor: 'black' }} type="button" value="submit" title="Click here to update your password." onClick={ this.handleSubmitPassword } />
                    </div> {/* editing password like this may be unwise*/}
                    <div style={entry}>
                        <div> New Email: </div> <input type="text" onChange={ this.handleEditEmail } />
                        <input style={{ borderRadius: 30, color: 'white', backgroundColor: 'black' }} type="button" value="submit" title="Click here to update your email." onClick={ this.handleSubmitEmail } />
                    </div>
                    <div style={entry}>
                        <div> New Avatar: </div>
                        <img src={this.state.new_avatar} alt="Enter a valid url" height='84' width='84' /><br/>
                        <input type="text" onBlur={ this.handleEditAvatar } />
                        <input style={{ borderRadius: 30, color: 'white', backgroundColor: 'black' }} type="button" value="submit" title="Click here to update your avatar." onClick={ this.handleSubmitAvatar } />
                    </div>
                </div>
            </div>
        );
    }
}

export default withCookies(EditProfile)

const entry = {
    flex: 1,
    flexDirection: 'row'
}

const form = {
    flex: 1,
    flexDirection: 'column',
    padding_left: 5
}

const view = {
    flex: 1,
    flexDirection: 'column'
}

const section = {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    borderRadius: 4,
    boxShadow: '2px 2px 2px lightgrey',
    marginBottom: 20,
    backgroundColor: 'white'
} 