import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { CookiesProvider} from 'react-cookie'
import Reducer from './Redux/Reducer'
import { combineReducers } from 'redux'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

const combinedReducers = combineReducers({Reducer})

const store = createStore(combinedReducers)

ReactDOM.render((<CookiesProvider>
                    <Provider store={store}>
                        <App/>
                    </Provider>                
                </CookiesProvider>) , document.getElementById('root'));
registerServiceWorker();
