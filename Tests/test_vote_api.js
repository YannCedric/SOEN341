const chaiAsPromised = require('chai-as-promised')
const chai = require('chai')
const {vote} = require('../src/JS/Requests')

// chai.use(chaiAsPromised)
// const assert = require("chai").assert;

const expect = require("chai").expect;
const should = require("chai").should();

const prodFlag = process.argv[3]
const vote_url = prodFlag === "--prod" ? 'http://communiqate.club:4006/vote' : 'http://vote:3000/vote'

console.log('Flag: ',prodFlag || 'none')

var voteMock = {
    questionID: null,
    answerID: null,
    username: null,
    voteValue: null
} 

describe('Vote API', function() {
    it('Missing questionID', async () => {
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote refused. Request body is missing questionID.')
        ).catch(pleaseNoError )
    });
    it('Missing username', async () => {
        voteMock.questionID = '000011112222';
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote refused. Request body is missing username.')
        ).catch(pleaseNoError )
    });
    it('Missing voteValue', async () => {
        voteMock.username = 'TESTING';
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote refused. Vote value must be -1, 0 or 1')
        ).catch(pleaseNoError )
    });
    it('voteValue not a number', async () => {
        voteMock.voteValue = 'voteValue';
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote refused. Vote value must be -1, 0 or 1')
        ).catch(pleaseNoError )
    });
    it('voteValue greater than 1', async () => {
        voteMock.voteValue = 2;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote refused. Vote value must be -1, 0 or 1')
        ).catch(pleaseNoError )
    });
    it('voteValue smaller than -1', async () => {
        voteMock.voteValue = -2;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote refused. Vote value must be -1, 0 or 1')
        ).catch(pleaseNoError )
    });
    it('question not found', async () => {
        voteMock.voteValue = 1;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Question could not be found')
        ).catch(pleaseNoError )
    });
    it('Vote set (question)', async () => {
        voteMock.questionID = '5a996bbd40bb890011577348';
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote value set')
        ).catch(pleaseNoError )
    });
    it('Vote updated (question)', async () => {
        voteMock.voteValue = -1;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote value updated')
        ).catch(pleaseNoError )
    });
    it('Vote deleted (question)', async () => {
        voteMock.voteValue = 0;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote deleted')
        ).catch(pleaseNoError )
    });
    it('Vote not found (question)', async () => {
        voteMock.voteValue = 0;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote could not be deleted because it was not found')
        ).catch(pleaseNoError )
    });
    it('Answer not found', async () => {
        voteMock.answerID = 'answerID';
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Answer could not be found')
        ).catch(pleaseNoError )
    });
    it('Vote set (answer)', async () => {
        voteMock.answerID = '5a997009af66690011aee967';
        voteMock.voteValue = 1;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote value set')
        ).catch(pleaseNoError )
    });
    // it('Vote updated (answer)', async () => {
    //     voteMock.voteValue = -1;
    //     await vote(vote_url, voteMock).then(
    //         data => expect(data.response).to.be.equal('Vote value updated')
    //     ).catch(pleaseNoError )
    // });
    it('Vote deleted (answer)', async () => {
        voteMock.voteValue = 0;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote deleted')
        ).catch(pleaseNoError )
    });
    it('Vote not found (answer)', async () => {
        voteMock.voteValue = 0;
        await vote(vote_url, voteMock).then(
            data => expect(data.response).to.be.equal('Vote could not be deleted because it was not found')
        ).catch(pleaseNoError )
    });
});
function pleaseNoError( err ) {  console.log('-----------Error Start----------- \n' + err +'\n -----------Error End-----------'); should.equal(err, undefined)  }