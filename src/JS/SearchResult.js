import React, {Component} from 'react'
import {search} from './Requests'
import Post from './Post'
import  {searchPosts} from './Requests'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {increment, search as dispatchSearch} from '../Redux/Actions'

const mapStateToProps = state => {return {searching : state.Reducer.searching}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {dispatchSearch: dispatchSearch}, dispatch)


class SearchResult extends React.Component{
    constructor(props){

        super(props);
        this.state = {post: [], loading: false};
    }

    componentDidMount(){
        this.props.searching ? this._fetchPosts() : null
    }

    componentWillReceiveProps(){
        this.props.searching ? this._fetchPosts() : null
    }
    
    _fetchPosts(){ 
        const { key } = this.props.match.params
        this.setState({loading: true})

        searchPosts(key, 'http://communiqate.club:4000/search').then(res => {
                this.setState({loading: false})
                this.props.dispatchSearch(false)
                this.setState({post: []})
                this.setState({post: res.post})
                console.log(res.post)
        }).catch( err => {alert('problem fetching')} )
    }

    handleSubmit(event) {
        
    }

    render(){
        const {post: posts, loading} = this.state

        return(
            <div style={main}>
                {loading ? 'Loading ...' : ''}
                {posts ? posts.map(post => <Post withAnswers={false} matching={false} _id={post._id}/>) : 'no posts.'}
            </div>
        );
    }
}
export default connect(mapStateToProps, matchDispatchtoProps)(SearchResult)

const main={
    width: '80%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height:'90%',
}