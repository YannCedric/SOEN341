import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Logo from './Logo'
import '../App.css'

import {withCookies} from 'react-cookie'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {increment, search as dispatchSearch, logout} from '../Redux/Actions'

const mapStateToProps = state => {return {searching : state.Reducer.searching, logged: state.Reducer.logged, visitor: state.Reducer.visitor}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {dispatchSearch: dispatchSearch, logout: logout}, dispatch)

const profileUrl= "https://cdn.onlinewebfonts.com/svg/img_507357.png"
const notifUrl=  "https://image.flaticon.com/icons/png/512/3/3513.png"
const newPostUrl="http://cdn.onlinewebfonts.com/svg/img_262791.png"

class Navbar extends Component {
    constructor(){
        super()

        this.state = {key: 'none'}
    }

    render(){
        const { size, cookies, visitor} = this.props

        return (
            <div id="nav" style={main}>
                <Logo nav size={1.2} />
                
                <div style={{width: '50%', alignSelf:'center',height: '80%', display: 'flex', alignItems: 'center'}}>
                    <input placeholder={'Search for a question using tags...'} style={search} onChange={ e => this.setState({key: e.target.value})}/> 
                    <Link to={`/feed/search/${this.state.key}`}><button onClick={()=> {this.props.dispatchSearch(true)}} style={button}>🔍</button></Link>
                </div>
                
                <div style={links(visitor ? 'hidden' : 'visibility')}>
                    <Link to="/feed/newpost"><div className="link"><img style={img} src={newPostUrl}/></div></Link>
                    <div className="link"><img style={img} src={notifUrl}/></div> 
                    <Link to="/feed/profile"><div className="link"><img style={img} src={profileUrl}/></div> </Link>
                    <Link to="/landing" ><button onClick={()=> {cookies.remove('logged') ; cookies.remove('username'); this.props.logout()}}><div className="link">Logout</div></button></Link>
                </div>

                <div id="logbuttons" style={links(!visitor ? 'hidden' : 'visible')}>
                    <Link to="/sign-In" ><button name="login"><div className="link">Login</div></button></Link>
                    <Link to="/sign-up" ><button name="signup"><div className="link">Sign-up</div></button></Link>
                </div>                             
            </div>
        )
    }
} 
export default connect(mapStateToProps, matchDispatchtoProps)(withCookies(Navbar))

const button = {
width: 40,
height: 42,
fontSize: 20,
borderRadius: 4
}


const img = {
    height: 30,
    width: 30,

}

const links = (visibility) => {
    return {
        width: '15%',
        fontSize: 15,
        display: 'flex', 
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        visibility: visibility
    }
}

const search = {
    width: '70%',
    height: 29,
    borderRadius: 2,
    fontSize: 15,
    padding: 5,

}

const main = {
    // backgroundColor: 'rgba(255,255,255,1)',
    backgroundColor: 'white',
    boxShadow: '0px -5px 20px black',
    display: 'flex',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'space-between',
    // paddingHorizontal: 100,
    height: 70,
    width: '100%',
  }
 