const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//using mongoose to establish a connection to the database.
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
const db = mongoose.connection;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


const Schemas = require('./schemas');

//Usage: GET /profile/<username>
//Returns:
// - 404: user not found
// - 200: JSON data following the structure from
//        https://gitlab.com/YannCedric/SOEN341/wikis/Design-Document#profile-json
app.get('/profile/:username', function (req, res){

    var username = req.params.username;

    //Find the user
    Schemas.User.findOne({'username': username}, function(err, user){

        if (err || user == null)
        {
            //Not found
            res.status(404);
            res.send(JSON.stringify({message: "User not found..."}));
        }
        else
        {
            //Found, return select data from user document.
            res.status(200);
            res.send(JSON.stringify({
                email: user.email,
                name: user.name,
                profile_pic: user.profile_pic
            }));
        }
    });
});


//Usage: PUT /profile/<username>
//Body: JSON data with the updated profile info following the structure from
//      https://gitlab.com/YannCedric/SOEN341/wikis/Design-Document#profile-json
//Returns:
// - 404: user not found
// - 200: update successful
app.post('/profile/:username', function (req, res) {

    var username = req.params.username;

    //Find the user
    Schemas.User.findOne({'username': username}, function(err, user){

        if (err || user == null)
        {
            //Not found
            res.status(404);
            res.send(JSON.stringify({message: "User not found..."}));
        }
        else
        {
            //Found, update select data from user document.
            Schemas.User.update({username: username}, {
                email: req.body.email,
                name: req.body.name,
                profile_pic: req.body.profile_pic
            }, {upsert: true}, function (err) {
                if (err)
                {
                    res.status(500);
                    res.send(JSON.stringify({message: "Failed to update the user data..."}));
                }
            });
            res.status(200);
            res.send();
        }
    });

});

const listener = app.listen(3000, function(){
    console.log("Hello World! I\'m running! on port "+listener.address().port);
});