const Nightmare = require('nightmare')
const chai = require('chai')
const expect = chai.expect
const should = chai.should

const prod_url = "http://localhost:3000"

const prodFlag = process.argv[3]
const url = prodFlag === "--dev" ? 'http://127.0.0.1:3000' : prod_url

describe('Cookies Tests', () => {
  it('should check cookie value to correspond to username', function(done) {
    this.timeout('10s')

    const nightmare = Nightmare({show: true})
    nightmare
      .goto(url)
      .wait('button[name=signin]')
      .click('button[name=signin]')
      .wait('#uname')
      .wait('#pass')
      .type('#uname', 'bbc')
      .type('#pass', 'pass')
      .click('#submit')
      .type('body', '\u000d')
      .cookies.get('username')
      .end()
      .then( cookie => {
        try{
            expect(cookie.value).to.equal('bbc'); 
            done()
        }catch(e) {
            done()            
        }
        })
  })
})

describe('Anonymous Login Test', () => {
  it('Checks the navbar for buttons', function(done) {
    this.timeout('10s')

    const nightmare = Nightmare({show: true})
    nightmare
      .goto(url)
      .wait('button[name=visitor]')
      .click('button[name=visitor]')
      .evaluate(() => {
        const visibility = document.querySelector('#logbuttons').style.visibility
        const buttonsignup = document.querySelector('button[name=signup]').style.visibility
        const buttonlogin = document.querySelector('button[name=login]').style.visibility

        return {visibility,buttonsignup,buttonlogin}
      })
      .end()
      .then( ({visibility,buttonsignup,buttonlogin}) => {
        // console.log(visibility)
        try{
            expect(visibility).to.equal('visible'); 
            expect(buttonsignup).to.equal('visible'); 
            expect(buttonsignup).to.equal('visible'); 
            done()
        }catch(e) {
            done()            
        }
        })
  })
})

