import React from 'react';

export class Voting extends React.Component {
    constructor() {
        super();

        this.state = {
            votes: 0,
        };

        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
    }

    render() {
        return (
            <div>
                <div>{this.state.votes}</div>
                <button className="upvote" onClick={this.increment}>Upvote</button>
                <button className="downvote" onClick={this.decrement}>Downvote</button>
            </div>
        );
    }

    increment() {
        this.setState({
            votes: this.state.votes + 1,
        });
    }

    decrement() {
        this.setState({
            votes: this.state.votes - 1,
        });
    }
}