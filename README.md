
[http://communiQAte.club](http://communiQAte.club)
===============

by group UA3:

### Scrum Master:
* David Payette --- david.payette@outlook.com

### Backend:
* Alex Frappier Lachapelle --- alexfl1024@gmail.com
* Amal Javed --- javedamal@hotmail.com
* Anthony Iatropoulos --- anthonyiatropoulos@hotmail.com
* William-Andrew Lussier --- william.lussier115@hotmail.com

### Frontend:
* Yann Cedric --- yanncedrik@gmail.com
* Nicholas Iatropoulos --- nicholasiatropoulos@hotmail.com
* George Hatem --- ghy243@gmail.com

## Project Goal

* Create a Q&A website where users can ask and answer questions of any topic.

## Core Features

* Ask and answer questions
* Vote on the answers
* Accept and reject answers

## Additional Features

* Allow users to create an account
* Add comments on questions and answers
* Edit questions & answers with edit history and timestamps
* Allow to ask and answer questions in an anonymous fashion
* Allow for users to search for questions and answers based on title and content including a tag system to improve search granularity
* Allow users to gain reputation based on the positive or negative votes on their questions and answers

## References and used tools

* [AWS](https://aws.amazon.com/console/) for the web hosting
* [NodeJS](https://nodejs.org/en/) for the backend framework
* [ExpressJS](https://expressjs.com/) for the backend server
* [ReacJS](https://reactjs.org/) for the frontend rendering
* [Docker](https://www.docker.com/) for deployment
* [Mongo DB](https://www.mongodb.com/) for backend database


---
# API list

 - **Search API** for posts on port **4000** --- (*William-Andrew Lussier*)
---
# Developement
## Front-End

After cloning the project, 

 1. First run `npm install` within the project's main directory
 2. Then run `npm run dev` to start the developement server
 3. To test local buid, run `npm run single-test`
 4. To continuously run tests during developement, run `npm run test`

 ## Back-End 

 For running E2E tests,

 0. Make sure docker is running
 1. Move into nightwatch folder : `cd nightwatch`
 2. Run the following command and make sure docker is installed `docker-compose run --rm  nightwatch`
 3. Run `docker-compose down` in the nightwatch folder 

 <!-- docker run -v $(pwd)/Tests:/app --rm -p 8889:8889 nightmarejs-webservice -->
 <!-- docker build -t nightmarejs-webservice . -->

 > [TODO]