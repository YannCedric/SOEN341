const axios = require('axios')

module.exports = {

    searchPosts(key,url,_id){

    return axios.post( url , {
        search: key,
        _id
        })
        .then(function (response) {
            const {data} = response
            return data;
        })
    } 
,
    getProfile(url,username){
    const endpoint = url+'/'+username
    return axios.get(endpoint)
        .then(function (response) {
            const {data} = response
            return data;
        })
    }
,
    setProfile(url,username,{email, name, profile_pic}){
    const endpoint = url+'/'+username
    return axios.post(endpoint,{email, name, profile_pic})
        .then(function (response) {
            return response;
        })
    }
,
    signUp(data,url){

    return axios.post( url , data)
      .then(function (response) {
          const {data} = response
          return data;
      })
    }
,
    signIn(data,url){

        return axios.post( url , data)
          .then(function (response) {
              const {data} = response
              return data;
          })
        }
,

    post(data, url) {
        return axios.post( url , data)
          .then(function (response) {
              const {data} = response
              return data;
          })
        }
,
    sendAnswer(data, url) {
        return axios.post( url , data)
          .then(function (response) {
            const {data} = response
            return data;
        })
    }
,
    acceptRefuseAnswer(data, url) {
        return axios.post( url , data)
            .then(function (response) {
            const {data} = response
            return data;
        })
    }
,
    vote(url, requestBody) {
        return axios.post(url, requestBody)
            .then(function (response) {
            const {data} = response
            return data;
        })
    }
,
    vote(url, data) {
        return axios.post( url , data)
          .then(function (response) {
            const {data} = response
            return data;
        })
    }
}

function vote(url, data){
    return axios.post( url , data)
      .then(function (response) {
        const {data} = response
        return data;
    })
}

const data = {
        "questionID" : "5ab25ee326536c00117d0d77",
        "answerID" : "5ab51c7dd2519200116f143e",
        "username" : "yannnn",
        "voteValue" :  1
    } 

vote('http://communiqate.club:4006/vote',data).then(console.log)