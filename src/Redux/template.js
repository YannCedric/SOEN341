import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {increment} from '../Redux/Actions'

const mapStateToProps = state => {return {value : state.Reducer.value}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {increment: increment}, dispatch)

export default connect(mapStateToProps, matchDispatchtoProps)(Classname)