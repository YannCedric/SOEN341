import React, { Component } from 'react'
import './App.css'

import LandingPage from './JS/LandingPage'
import SignIn from './JS/SignIn'
import SignUp from './JS/SignUp'
import Feed from './JS/Feed'
import { withCookies, Cookies } from 'react-cookie'

import {HashRouter as Router, Route, Redirect, IndexRedirect} from 'react-router-dom'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {increment, search as dispatchSearch, login} from './Redux/Actions'

const mapStateToProps = state => {return {name: state.Reducer.name, logged: state.Reducer.logged,}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {login: login}, dispatch)


class App extends Component {

  constructor(){
    super()

    this.state = {}
  }

  componentDidMount(){
    const {cookies} = this.props
    const logged =  cookies.get('logged')
    const name =  cookies.get('username')      

    logged ? this.props.login(name) : null
      
        
  }

  render() {
    const { logged , username} = this.props
    const { cookies } = this.props

    return (
      <Router  >
        <div style={main} >
            <Redirect from="/" to= {logged ? "/feed" : "/landing"} />
            <Route path="/landing" component={LandingPage} />
            <Route path="/sign-up" component={() => <SignUp onSignUp={username => { cookies.set('logged', true);cookies.set('username', username); this.props.login(username) }   } />}/>
            <Route path="/sign-in" component={() => <SignIn onSignIn={username => { cookies.set('logged', true);cookies.set('username', username); this.props.login(username) }  } />}/>
            <Route path="/feed" component={()=><Feed username={username}/>}/>
        </div>
      </Router>
    );
  }
}

export default connect(mapStateToProps, matchDispatchtoProps)(withCookies(App))

const main = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0, 
  bottom: 0,
}


