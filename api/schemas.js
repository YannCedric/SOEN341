var mongoose = require('mongoose');
var dbConfig = require('../config/dbConfig');
mongoose.connect(dbConfig.url);
var db = mongoose.connection;


var user = mongoose.Schema({
    username: String,
    password: String,
    email: String,
    reputation: Number,
    name: {type: String, default: ""},
    profile_pic: {type: String, default: ""}
});

var nestedvote = mongoose.Schema({
        vote: Number, // -1 or 1
        username: String
});

var answer = mongoose.Schema({
    anonymous: Boolean,
    answer: String,
    username: String,
    anonymous: Boolean,
    accepted: Boolean,
    creationTime: String,
    votes: {
        tally: Number,
        users: [{type: mongoose.Schema.Types.ObjectId, ref: 'NestedVote'}]
    }
});

var question = mongoose.Schema({
    anonymous: Boolean,
    title: String,
    question: String,
    votes: {
        tally: Number,
        users: [{type: mongoose.Schema.Types.ObjectId, ref: 'NestedVote'}]
    },
    answers: [{type: mongoose.Schema.Types.ObjectId, ref: 'Answer'}],
    tags: [String],
    username: String,
    anonymous: Boolean,
    creationTime: String
});

var comment = mongoose.Schema({
    comment: String,
    username: String,
    anonymous: Boolean,
    creationTime: String,
})

module.exports = {
    
    User: db.model('User', user),
    NestedVote: db.model('NestedVote', nestedvote),
    Answer: db.model('Answer', answer),
    Question: db.model('Question', question),
    Comment: db.model('Comment', comment)
};