
module.exports = function (app, passport, db, bodyParser) {

    require('./post')(app, passport, db, bodyParser);



};



function isLoggedIn(req, res, next) {

    if (req.isAuthenticated())
        return next();

    //can be replaced later with a 403
    res.redirect('/notloggedin')
}
