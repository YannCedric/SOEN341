import React, { Component } from 'react'
import '../App.css'

import Logo from './Logo'
import {Link} from 'react-router-dom'
import { signIn as signInMethod } from './Requests'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {setVisitor} from '../Redux/Actions'

const mapStateToProps = state => {return {visitor: state.Reducer.visitor}}
const matchDispatchtoProps = dispatch =>  bindActionCreators( {setVisitor: setVisitor}, dispatch)

class SignIn extends Component {
    constructor(){
        super()
        this.state = {mounted : false, userName: ' ', password: ' '}        
    }
    
    componentDidMount(){

        setTimeout(() => {
            this.setState({mounted: true})
        }, 100);
    }

    processLogin(){
        const {userName , password} = this.state

        signInMethod({userName, password}, 'http://communiqate.club:4002/signin')
        .then(res => {
            alert(res)
            if(res.includes('Welcome')){
                this.props.setVisitor(false)               
                this.props.onSignIn(userName)
            }
        })
    }

  render() {
      const { mounted, userName, password} = this.state
    return (
      <div style={main(mounted)}>
        <Logo/>
        <div style={box}>
            <a style={labels}>Username</a>
            <input id="uname" placeholder="superman" style={input} onChange={e => this.setState({userName: e.target.value})}/>

            <a style={labels}>Password</a>
            <input id="pass" placeholder="try 'password'" type="password" style={input} onChange={e => this.setState({password: e.target.value})}/>

            
            <button id="submit" style={button} onClick={() => this.processLogin()}>
                login
            </button>
            <div style={separator}/>

            <Link to="/sign-up" style={{fontSize: 15,}}>I don't have an account yet</Link>

            <a href="" onClick={()=> alert('too bad..')} style={{fontSize: 15,}}>I forgot my password</a>
            
        </div>
      </div>
    );
  }
}
export default connect(mapStateToProps, matchDispatchtoProps)(SignIn)

const separator = {
    height: 0.5,
    width: '60%',
    backgroundColor: 'black',
    opacity: 0.2,
    margin: 15,
    // boxShadow: '0px 0px 3px black',   
}

const labels={
    fontSize: 14,
}

const main = (on) => {
    return {display: 'flex',
    flexDirection: 'column',
    justiFyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    opacity: on ? 1 : 0,
    transition: 'all 0.5s ease-in-out',
    transform: on ? 'translateY(10px)' : ''
    }
}


const box = {
    marginTop: 10,
    width: 250,
    height: 255,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    border: '1px solid rgba(0,0,0,0)',
    boxShadow: '0px 0px 5px black',
    padding: 5,
    borderRadius: 2,
    fontFamily: 'Arial',
    backgroundColor: 'rgba(255,255,255,0.8)'

}

const input = {
    width: '80%',
    borderRadius: 3,
    height: 20,
    fontSize: 15,
    padding: 5,
}
 
const button = {
    marginTop: 15,
    height: 30,
    width: '80%',
    borderRadius: 5,
    fontSize: 15,
    // boxShadow: '0px 0px 10px black',
    

}