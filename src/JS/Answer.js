import React, {Component} from 'react';
import '../App.css'
import {vote} from './Requests'
import { withCookies } from 'react-cookie'

const voteApi = 'http://communiqate.club:4006/vote'

class Answer extends React.Component{
    constructor(props){
        super(props);

        this.state = {tally: 0};

    }

    componentDidMount(){
        console.log(this.props.ans)
        this.setState({tally: this.props.tally})
    }
    
    // componentDidUpdate(){
    //     this.setState({tally: this.props.tally || 0})
    // }

    // {
    //     "questionID" : "5a996bbd40bb890011577348",
    //     "answerID" : "5a997009af66690011aee967",
    //     "username" : "bbc",
    //     "voteValue" : -1
    // }   
    voteUp(){
        const { post_id, ans_id, username , answer, poster_name, visitor, cookies} = this.props
        const author =  cookies.get('username') 

        console.log('post details', {post_id, ans_id, username})
        
        const data = {
            "questionID" : post_id,
            "answerID" : ans_id,
            "username" : username,
            "voteValue" : 1
        } 
        console.log(data)
        // console.log({author,username})
        if(author != username)
        vote(voteApi,data).then(res => {
            if(res.tally){
                alert('vote accepted')
                setTimeout(()=>{
                    console.log('voted', res.tally)
                    this.props.reload()
                }, 1000)
            }
            else
                alert(res.response)
        }).catch(res => alert('error voting'))
        else
            alert('Can\'t vote on your own answer!')

    }

    voteDown(){
        const { post_id, ans_id, username , answer, poster_name, visitor, cookies} = this.props
        const author =  cookies.get('username') 

        console.log('post details', {post_id, ans_id, username})
        
        const data = {
            "questionID" : post_id,
            "answerID" : ans_id,
            "username" : username,
            "voteValue" : -1
        } 
        console.log(data)
        // console.log({author,username})
        if(author != username)
        vote(voteApi,data).then(res => {
            if(res.tally){
                alert('vote accepted')
                setTimeout(()=>{
                    console.log('voted', res.tally)
                    this.props.reload()
                }, 1000)
            }
            else
                alert(res.response)
        }).catch(res => alert('error voting'))
        else
            alert('Can\'t vote on your own answer!')
    }

    render(){
        const {username , answer, poster_name, visitor} = this.props
        const { tally } = this.state
        
        return(
            <div style={main}>
                <div style={images(visitor ? 'hidden': 'visible')}>
                    <img onClick={ () => this.voteUp() } className="like" src="http://www.clker.com/cliparts/f/0/t/Y/n/t/thumbs-up-icon-green-th.svg.hi.png"/>
                    <div>{this.props.tally}</div>
                    <img onClick={()=> this.voteDown() } className="like" src="http://www.voluntas.co.uk/wp-content/uploads/2013/11/Thumb-down-in-Red.png"/>
                </div>
                <div style={box}>
                    <div style={{fontSize: 14, color: 'black', fontWeight: 'bold', fontFamily: 'bookman', marginBottom: 10, display: 'flex', flexDirection: 'row', alignSelf: 'flex-end'}}><div style={{fontSize: 10, marginRight: 5, alignSelf:'center', fontWeight: 'lighter',fontFamily: 'courrier',color: 'lightgrey'}}>{'from'}</div>{username}</div>
                    <div style = {{marginLeft: 15,fontSize: 20, color: 'black', fontFamily: 'courrier',fontWeight: 'lighter',}}>{answer}</div>
                </div>
                {/* <img className="accept" src="https://cdn0.iconfinder.com/data/icons/large-glossy-icons/512/Apply.png" /> */}
            </div>
        );
    }
}

export default withCookies(Answer)

const main = {
    display: 'flex',
    width: '70%',
    flexDirection: 'row',
    alignItems: 'space-around',
}

const box = {
    display: 'flex',
    width: '80%',
    minHeight: 110,
    flexDirection: 'column',
    alignItems: 'space-around',
    backgroundColor: 'white',
    borderTop: '1px solid #f2f2f2',
    boxShadow: '1px 2px 1px lightgrey',    
    margin: 5,
    padding: 10,
}
const images = (visibility)=>{ 
    return {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        visibility: visibility
        
    }

 }