export  function increment(value) {
    return {
      type: 'INCREMENT',
      payload: value,
    }
  }

export  function search(value) {
    return {
      type: 'SEARCH',
      payload: value,
    }
  }

export  function logout() {
    return {
      type: 'LOGOUT',
    }
  }

export  function setVisitor(value) {
    return {
      type: 'VISITOR',
      payload: value
    }
  }

export  function login() {
    return {
      type: 'LOGIN',
    }
  }