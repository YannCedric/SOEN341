const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const mongodb = require('mongodb');

//db using mongoose
mongoose.connect('mongodb://communiqate.club:3000/communiqate');
const db = mongoose.connection;

// Init App
const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Set Port
app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function(){
	console.log('Server started on port '+app.get('port'));
});



// API for listing Answers
app.post('/listAnswers', function(req, res) { 

    const post_id = mongodb.ObjectId(req.body.post_id);
    
    //Get the post 
    db.collection('posts').findOne({_id: post_id}, function(err, result){
        
        if(err) throw err;
                
        if(result != null){
            res.json({answers: result.answers, searchStatus: 'Found'}); //Get Answers
        }
        else 
            res.json({searchStatus: 'Not Found'});
    });
      res.end;
});